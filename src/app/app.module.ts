import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule,Routes } from '@angular/router';

import { AppComponent } from './app.component';

import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';

import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { VideosComponent } from './videos/videos.component';
import { PopupvideoComponent } from './popupvideo/popupvideo.component';
import { ArticlesComponent } from './articles/articles.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ArticledetailsComponent } from './articledetails/articledetails.component';
import { PopuploginComponent } from './popuplogin/popuplogin.component';
import { PopupsignupComponent } from './popupsignup/popupsignup.component';
import { PopupresetpasswordComponent } from './popupresetpassword/popupresetpassword.component';
import { PopupbookedsuccessComponent } from './popupbookedsuccess/popupbookedsuccess.component';
import { PopupcancelpolicyComponent } from './popupcancelpolicy/popupcancelpolicy.component';
import { PopupconfirmloginComponent } from './popupconfirmlogin/popupconfirmlogin.component';
import { PopupconfirmsignupComponent } from './popupconfirmsignup/popupconfirmsignup.component';
import { PopupconfirmpaymentComponent } from './popupconfirmpayment/popupconfirmpayment.component';
import { PopupnewpasswordComponent } from './popupnewpassword/popupnewpassword.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { MakeappointmentComponent } from './makeappointment/makeappointment.component';

import {Observable} from "rxjs";
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import { FlatpickrModule } from 'angularx-flatpickr';




const routes: Routes = [
	{path:'',component:HomepageComponent},
  {path:'navbar',component:NavbarComponent},
  {path:'aboutus',component:AboutusComponent},
  {path:'countactus',component:ContactusComponent},
  {path:'articles',component:ArticlesComponent},
  {path:'articledetails',component:ArticledetailsComponent},
  {path:'terms',component:TermsComponent},
  {path:'privacy',component:PrivacyComponent},
  {path:'myaccount',component:MyaccountComponent},
  {path:'videos',component:VideosComponent},
  {path:'makeappointment',component:MakeappointmentComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavbarComponent,
    FooterComponent,
    AboutusComponent,
    VideosComponent,
    PopupvideoComponent,
    ArticlesComponent,
    ContactusComponent,
    ArticledetailsComponent,
    PopuploginComponent,
    PopupsignupComponent,
    PopupresetpasswordComponent,
    PopupbookedsuccessComponent,
    PopupcancelpolicyComponent,
    PopupconfirmloginComponent,
    PopupconfirmsignupComponent,
    PopupconfirmpaymentComponent,
    PopupnewpasswordComponent,
    TermsComponent,
    PrivacyComponent,
    MyaccountComponent,
    MakeappointmentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatRadioModule,
		NgbModule,
		// FlatpickrModule.forRoot(),
		CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
