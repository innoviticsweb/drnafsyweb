import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

public emptysession = false ;
public fullsession = false ;
public editprofile = false ;
public hide :any = true;


  constructor() { }

  ngOnInit() {

  this.fullsession = true ;


  }


  toggle(type : any ) {

    switch(type){

      case 'emptysession' :
        this.emptysession = true ;
        this.fullsession = false;
        this.editprofile = false;
        break;

      case 'fullsession' :
      	this.emptysession = false;
        this.fullsession =  true;
        this.editprofile = false;
        break;

      case 'editprofile' :
      	this.emptysession = false;
        this.fullsession =  false;
        this.editprofile = true;
        break;   



      default :
        break;

    }
  }


}
