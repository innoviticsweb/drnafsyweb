import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupbookedsuccessComponent } from './popupbookedsuccess.component';

describe('PopupbookedsuccessComponent', () => {
  let component: PopupbookedsuccessComponent;
  let fixture: ComponentFixture<PopupbookedsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupbookedsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupbookedsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
