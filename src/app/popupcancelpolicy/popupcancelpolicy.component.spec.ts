import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupcancelpolicyComponent } from './popupcancelpolicy.component';

describe('PopupcancelpolicyComponent', () => {
  let component: PopupcancelpolicyComponent;
  let fixture: ComponentFixture<PopupcancelpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupcancelpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupcancelpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
