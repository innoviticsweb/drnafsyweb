import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popupcancelpolicy',
  templateUrl: './popupcancelpolicy.component.html',
  styleUrls: ['./popupcancelpolicy.component.css']
})
export class PopupcancelpolicyComponent implements OnInit {

  disabledAgreement: boolean = true;
  constructor() { }

  ngOnInit() {
  }

  agree(){
    document.getElementById('closepress').click();
  }
  changeCheck(event){
    this.disabledAgreement = !event.checked;
  }

}
