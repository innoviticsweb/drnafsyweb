import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupconfirmloginComponent } from './popupconfirmlogin.component';

describe('PopupconfirmloginComponent', () => {
  let component: PopupconfirmloginComponent;
  let fixture: ComponentFixture<PopupconfirmloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupconfirmloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupconfirmloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
