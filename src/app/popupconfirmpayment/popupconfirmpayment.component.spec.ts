import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupconfirmpaymentComponent } from './popupconfirmpayment.component';

describe('PopupconfirmpaymentComponent', () => {
  let component: PopupconfirmpaymentComponent;
  let fixture: ComponentFixture<PopupconfirmpaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupconfirmpaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupconfirmpaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
