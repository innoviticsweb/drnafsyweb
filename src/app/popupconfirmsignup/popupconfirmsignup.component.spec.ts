import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupconfirmsignupComponent } from './popupconfirmsignup.component';

describe('PopupconfirmsignupComponent', () => {
  let component: PopupconfirmsignupComponent;
  let fixture: ComponentFixture<PopupconfirmsignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupconfirmsignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupconfirmsignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
