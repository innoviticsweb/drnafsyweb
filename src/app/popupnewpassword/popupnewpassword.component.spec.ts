import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupnewpasswordComponent } from './popupnewpassword.component';

describe('PopupnewpasswordComponent', () => {
  let component: PopupnewpasswordComponent;
  let fixture: ComponentFixture<PopupnewpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupnewpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupnewpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
